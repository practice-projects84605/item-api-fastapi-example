"""
Authorization utilities

Some code has been taken from FastAPI documentation:
https://fastapi.tiangolo.com/tutorial/security/oauth2-jwt/
"""
from datetime import datetime, timedelta, timezone


from fastapi import Depends
from fastapi import HTTPException, status
from fastapi.encoders import jsonable_encoder
from fastapi.security import OAuth2PasswordBearer

from jose import JWTError, jwt, ExpiredSignatureError
from passlib.context import CryptContext

from sqlalchemy.orm import Session

from services import dependencies, service_functions as serv_fn
from schemas.user import UserInDB
from models.user import User as UserModel


SECRET_KEY = "3a5d9f6d67f1071eb7d192e4ed7b5f96d102183b3e47439b12e56d11068a527d"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 5

# used to handle password hashing
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

# added to handle login with Oauth2
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/user/login")


def get_password_hash(password):
    """
    Hashing the given password
    """

    return pwd_context.hash(password)


def verify_password(plain_password, hashed_password):
    """
    Check if the given password(plain_password) generates the same hashed password
    """

    return pwd_context.verify(plain_password, hashed_password)


def get_user(db: Session, user_identifier: str) -> UserInDB | None:
    """Get user information saved into DB

    Args:
        db: Database session
        user_identifier: word to find a user into db

    Returns
        UserInDB | None
    """

    user = serv_fn.get_item_by_tag(
        db=db, word=user_identifier, model=UserModel, column_name="email"
    )

    if user is not None:
        return UserInDB(**jsonable_encoder(user))


def authenticate_user(
    db: Session, user_identifier: str, password: str, get_user_fn
) -> bool | UserInDB:
    """Check if user credential are correct
    Args:

        db: Database session
        user_identifier: word that works as a identifier to find a user into db
        password: user password
        get_user_fn: function to get user from DB

    Returns:

        bool | UserInDB
    """

    user = get_user_fn(db=db, user_identifier=user_identifier)

    if user is None:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict) -> str:
    """
    Create an access token for the user

    Args:
        data(dict): information to encode

    Returns:

        str: encoded string
    """

    data_to_encode = data.copy()

    expiration_date = datetime.now(timezone.utc) + timedelta(
        minutes=ACCESS_TOKEN_EXPIRE_MINUTES
    )

    data_to_encode.update({"exp": expiration_date})

    data_encoded = jwt.encode(
        claims=data_to_encode, key=SECRET_KEY, algorithm=ALGORITHM
    )

    return data_encoded


def get_current_user(
    token: str = Depends(oauth2_scheme),
    db: Session = Depends(dependencies.get_db),
) -> UserInDB | None:
    """Using a token and a connection get user from db and check if it is authorized
    and also if the expiration time from token has expired

    Args:
        token(str): User token
        db: Database session

    Returns:
        UserInDB | None
    """

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")

        if username is None:
            raise credentials_exception
        # token_data = TokenData(username=username)

    # catch error when token has expired
    except ExpiredSignatureError as e:
        credentials_exception.detail = "Token has expired"
        raise credentials_exception from e

    # something get wrong decoding the token
    except JWTError as e:
        raise credentials_exception from e

    # get user from DB
    user = get_user(db=db, user_identifier=username)
    if user is None:
        raise credentials_exception
    return user
