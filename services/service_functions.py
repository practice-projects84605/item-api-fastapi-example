"""
All operations to make over a database are placed here
"""
from typing import List
from pydantic import BaseModel
from sqlalchemy.orm import Session

from config.database import Base


def get_items(db: Session, model: Base) -> List:
    """Standard function to use with a sqlalchemy model
    Get all elements for a given model

    Args:
        model: schlalchemy model
        db: Database connection

    Returns
        List: All elements from the model
    """

    # get all elements from model
    result = db.query(model).all()

    return result


def get_item_by_id(
    db: Session, id_record: int, model: Base, id_column: str = "id"
) -> Base | None:
    """Standard function to use with a sqlalchemy model.

    Get a specific element for a given model based on an id

    Args:
        db: Database connection
        id_record: id to find in database
        model: schlalchemy model
        id_column: name of the ID column (default is 'id')

    Returns
        record: a record for the type of 'model' | None
    """

    # Construct the filter condition dynamically based on the provided id_column
    filter_condition = getattr(model, id_column) == id_record

    # Query the database
    record = db.query(model).filter(filter_condition).first()

    return record


def get_items_by_tag(
    db: Session, word: str, model: Base, column_name: str = "id"
) -> List:
    """Standard function to use with a sqlalchemy model.

    Get a elements for a given model based on an word and a given column name

    Args:
        db: Database connection
        word: word to match with the info into db
        model: schlalchemy model
        column_name: name of the column (default is 'id')

    Returns
        record: list of records found
    """

    # Construct the filter condition dynamically based on the provided column name
    filter_condition = getattr(model, column_name) == word

    # Query the database
    record = db.query(model).filter(filter_condition).all()

    return record


def get_item_by_tag(
    db: Session, word: str, model: Base, column_name: str = "id"
):
    """Standard function to use with a sqlalchemy model.

    Get a specific element for a given model based on an word and a given column name

    Args:
        db: Database connection
        word: word to match with the info into db
        model: schlalchemy model
        column_name: name of the column (default is 'id')

    Returns
        record: record found or None
    """

    # Construct the filter condition dynamically based on the provided column name
    filter_condition = getattr(model, column_name) == word

    # Query the database
    record = db.query(model).filter(filter_condition).first()

    return record


def created_record(db: Session, model: Base, schema: BaseModel) -> None:
    """Standard function to use with a sqlalchemy model
    Create a new element for a given database model, using a pydantic schema

    Args:
        db: Database connection
        model: schlalchemy model
        schema: pydantic model

    Returns:
        None
    """

    # Insert a new element into the database
    db.add(model(**schema.model_dump()))

    # commit changes
    db.commit()


def update_record(db: Session, model: Base, schema: BaseModel) -> None:
    """Standard function to use with a sqlalchemy model
    Update a record for a table based in a given pydantic model

    Args:
        db: Database connection
        model: schlalchemy model
        schema: pydantic model

    Returns:
        None
    """

    # look over each element into pydantic model
    for key, value in schema.model_dump().items():
        # change values in database model based on pydantic ids
        setattr(model, key, value)

    # commit changes
    db.commit()


def delete_record(db: Session, record: Base) -> None:
    """Standard function to use with a sqlalchemy model
    Delete a record for a table based in a given record
    Args:
        record: database record

    Returns:
        None
    """

    # delete record from database
    db.delete(record)

    # commit the changes
    db.commit()


def delete_record_by_id(
    db: Session, id_record: int, model: Base, id_column: str = "id"
) -> None:
    """Standard function to use with a sqlalchemy model
    Delete a record for a table based in a given id and model

    Args:
        db: Database connection
        id_record: id to delete from database
        model: schlalchemy model

    Returns:
        None
    """

    # get id from database
    record = get_item_by_id(
        db=db, id_record=id_record, model=model, id_column=id_column
    )

    # delete record from database
    db.delete(record)

    # commit the changes
    db.commit()
