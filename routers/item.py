"""
Endpoints related to Item
"""

from typing import List
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi import APIRouter, Path, status, Depends

from sqlalchemy.orm import Session

from models.item import Item as ItemModel
from schemas.item import Item, ItemPrimaryKey, UpdateItem
from services import dependencies, service_functions as serv_fn, auth

item_router = APIRouter(
    tags=["item"],
    prefix="/item",
)


@item_router.post(
    "/auth",
    status_code=status.HTTP_201_CREATED,
    response_model=dict,
    dependencies=[Depends(auth.get_current_user)],
)
def insert_item_auth(item: Item, db: Session = Depends(dependencies.get_db)):
    """Insert a record into Items table"""

    serv_fn.created_record(db=db, model=ItemModel, schema=item)

    return JSONResponse(
        status_code=status.HTTP_201_CREATED,
        content={"message": "Record created successfully"},
    )


@item_router.post(
    "/",
    status_code=status.HTTP_201_CREATED,
    response_model=dict,
    # dependencies=[Depends(auth.get_current_user)],
)
def insert_item(item: Item, db: Session = Depends(dependencies.get_db)):
    """Insert a record into Items table"""

    serv_fn.created_record(db=db, model=ItemModel, schema=item)

    return JSONResponse(
        status_code=status.HTTP_201_CREATED,
        content={"message": "Record created successfully"},
    )


@item_router.get("/", response_model=List[ItemPrimaryKey])
def get_items(db: Session = Depends(dependencies.get_db)):
    """Get all items from item's table"""

    result = serv_fn.get_items(db=db, model=ItemModel)

    return JSONResponse(
        status_code=status.HTTP_200_OK, content=jsonable_encoder(result)
    )


@item_router.get("/{id}", response_model=ItemPrimaryKey)
def get_item_by_id(id: int, db: Session = Depends(dependencies.get_db)):
    """Get data from table using a given id"""

    record = dependencies.get_valid_record_by_id(
        db=db, id_record=id, model=ItemModel
    )

    return JSONResponse(
        status_code=status.HTTP_200_OK, content=jsonable_encoder(record)
    )


@item_router.get("/search/{brand}", response_model=List[ItemPrimaryKey])
def get_items_by_brand(
    brand: str = Path(..., max_length=20),
    db: Session = Depends(dependencies.get_db),
):
    """Search an item using brand column as a filter"""

    record = serv_fn.get_items_by_tag(
        db=db, word=brand, model=ItemModel, column_name="brand"
    )

    return JSONResponse(
        status_code=status.HTTP_200_OK, content=jsonable_encoder(record)
    )


@item_router.put("/", response_model=dict)
def update_item(
    item_id: int,
    item: UpdateItem,
    db: Session = Depends(dependencies.get_db),
):
    """Update a specified item from a given id"""

    record = dependencies.get_valid_record_by_id(
        db=db, id_record=item_id, model=ItemModel
    )

    serv_fn.update_record(db=db, model=record, schema=item)

    return JSONResponse(
        status_code=status.HTTP_200_OK, content={"message": "Record updated"}
    )


@item_router.delete("/", response_model=dict)
def delete_item(
    item_id: int,
    db: Session = Depends(dependencies.get_db),
):
    """Delete a specified item from a given id"""

    record = dependencies.get_valid_record_by_id(
        db=db, id_record=item_id, model=ItemModel
    )

    serv_fn.delete_record(db=db, record=record)

    return JSONResponse(
        status_code=status.HTTP_200_OK, content={"message": "Record eliminated"}
    )
