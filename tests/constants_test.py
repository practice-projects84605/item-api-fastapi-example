"""
Testing constants
"""

ITEM_TO_INSERT_1 = {
    "name": "Shampoo",
    "description": "hair care product comprising among other things of synthetic detergents designed to remove sebum and environmental dirt",
    "price": 12.5,
    "stock_quantity": 200,
}

ITEM_TO_INSERT_2 = {
    "name": "Computer",
    "description": "An electronic device that manipulates information, or data",
    "price": 350.12,
    "stock_quantity": 3,
}

ITEM_1 = {
    "name": "Shampoo",
    "price": 12.5,
    "stock_quantity": 200,
    "brand": None,
    "id": 1,
    "description": "hair care product comprising among other things of synthetic detergents designed to remove sebum and environmental dirt",
    "supplier": None,
}

ITEM_2 = {
    "name": "Computer",
    "price": 350.12,
    "stock_quantity": 3,
    "brand": None,
    "id": 2,
    "description": "An electronic device that manipulates information, or data",
    "supplier": None,
}

WRONG_ITEM_TO_INSERT = {
    "name": "",
    "description": "hair care product comprising among other things of synthetic detergents designed to remove sebum and environmental dirt",
    "price": 12.5,
    "stock_quantity": 200,
}