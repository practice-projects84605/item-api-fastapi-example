"""
User sqlalchemy model definition
"""

from sqlalchemy import Column, String

from config.database import Base


class User(Base):
    __tablename__ = "users"

    email = Column(String, primary_key=True)
    name = Column(String)
    hashed_password = Column(String)
